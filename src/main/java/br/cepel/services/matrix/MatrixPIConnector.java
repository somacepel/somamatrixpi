package br.cepel.services.matrix;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import br.cepel.common.meassurement.Waveform;
import br.cepel.fsw.FSWServiceContainer;

public class MatrixPIConnector {
	private static Logger logger = Logger.getLogger(MatrixPIConnector.class);

	FSWServiceContainer container;
	String jdbcUrlFull;
	Connection conn;

	public MatrixPIConnector(FSWServiceContainer container, String jdbcUrlFull) {
		this.container = container;
		this.jdbcUrlFull = jdbcUrlFull;
	}

	public void close() {
		// nothing to do
	}

	public Connection connect() {
		System.out.println("••• Verificando SQL Host");
		String serverHost = "";
		serverHost = jdbcUrlFull.trim().split("//")[1].split("/")[0];
		System.out.println("••• " + serverHost);
		InetAddress ip = null;
		try {
			ip = InetAddress.getByName(serverHost);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println("Begin -  mysql IP Addr = " + ip.getHostAddress());

		conn = connect(jdbcUrlFull);

		return conn;
	}

	private Connection connect(String JDBC_URL) {
		return connect(JDBC_URL, "Administrador", "!@Bravo@!");
	}

	private Connection connect(String JDBC_URL, String JDBC_USER, String JDBC_PASS) {
		System.out.println("Conectando");
		Connection connection = null;
		try {
			Class.forName("com.osisoft.jdbc.Driver");
			System.out.println("Driver Registered");
			connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
			System.out.println("Connection created for " + JDBC_USER);
			Statement stmt = connection.createStatement();
			ResultSet x = stmt.executeQuery("select major from piproductversion");
			while (x.next()) {
				x.getString("major");
			}

			return connection;
		} catch (Exception e) {
			logger.error("Problema de conexão com o PI: " + e.getMessage(), e);
		}
		return null;
	}

	public void disconnect() {
		try {
			conn.close();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public PIMeasureDetail[] getDataSnapshot(List<PITagMetaInfo> existingTags, StringPredicate filter) {
		// if (filter == null) {
		// throw new RuntimeException("••• Filtro não pode ser nulo");
		// }
		// TODO elaborar outra forma para construir esse filtro uma única vez
		List<String> tags = existingTags.stream().map((tag) -> "'" + tag.getTag() + "'").collect(Collectors.toList());
		StringPredicate predicate = StringPredicate.get("tag in (" + String.join(",", tags) + ")");
		logger.info("••• Predicate = " + predicate);

		Statement stmt = null;
		ResultSet resultSet = null;
		PIMeasureDetail[] piMeasuresDetail = new PIMeasureDetail[existingTags.size()];

		try {
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resultSet = stmt.executeQuery("select tag, time, value from pisnapshot where " + predicate);
			int actualIndex = 0;
			while (resultSet.next()) {
				String tag = resultSet.getString("tag");
				PITagMetaInfo tagMetaInfo = getPITagMetaInfoByTag(existingTags, tag);
				if (tagMetaInfo == null)
					continue;
				double[] values = new double[1];
				values[0] = resultSet.getDouble("value");
				Waveform waveform = new Waveform(resultSet.getDate("time").getTime(), 1.0, values);
				waveform.setQueueID(tagMetaInfo.getQueueId());
				logger.info("••• queueID: " + tagMetaInfo.getQueueId());
				// TODO estruturar para pegar do local correto o
				// DataSourceOrigin
				PIMeasureDetail measure = new PIMeasureDetail(tagMetaInfo.getDataSource(), tagMetaInfo.getTitle(), "PI",
						waveform);
				piMeasuresDetail[actualIndex++] = measure;
			}
			return piMeasuresDetail;
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

	public PITagMetaInfo getPITagMetaInfoByTag(List<PITagMetaInfo> existingTags, String tag) {
		for (PITagMetaInfo piTagMetaInfo : existingTags) {
			if (piTagMetaInfo.getTag().equals(tag)) {
				PITagMetaInfo tagMetaInfo = piTagMetaInfo;
				return tagMetaInfo;
			}
		}
		return null;
	}

	// Read the PI Schema
	public List<String> getTagList() {
		Statement stmt = null;
		ResultSet resultSet = null;
		ArrayList<String> tagList = new ArrayList<>();

		try {
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resultSet = stmt.executeQuery("select tag from pisnapshot");
			while (resultSet.next()) {
				tagList.add(resultSet.getString("tag"));
			}
			return tagList;
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

}
