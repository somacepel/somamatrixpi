package br.cepel.services.matrix;

import br.cepel.common.meassurement.Waveform;
import br.cepel.mngmt.inject.ManagedAttribute;
import br.cepel.services.waveform.WaveformService;
import business.ApplicationException;
import model.entity.plant.DataSource;
import org.apache.log4j.Logger;
import service.plant.DataSourceService;

import java.util.List;
import java.util.Map;

public class MatrixDumpDataService extends WaveformService {

	private static Logger logger = Logger.getLogger(MatrixDumpDataService.class);
	private DataSourceService dataSourceService;

	@ManagedAttribute(description = "Capacidade do buffer em waveforms", isReadable = true)
	private int bufferCapacity = 1;

	@Override
	protected void afterStop() throws Exception {
		// 
	}

	@Override
	protected void beforeStart() throws Exception {

		String scope = getScope();
		logger.info("••• scope = " + scope);

		// Obtendo a lista de Fontes de Dado para uma dada Fila de exemplo

		// PXI-1_M-2_C-[0-7]
		// PXI-1_M-5_C-[0-3]
		// PXI-1_M-7_C-[0-15]
		// PXI-1_M-8_C-[0-3]
		// PXI-1_M-9_C-[0-15]

		logger.info("••• queue = PXI-1_M-2_C-[0-7]");
		List<DataSource> dsList = dataSourceService.getDataSourcesByQueueName("PXI-1_M-2_C-[0-7]");

		for (DataSource ds : dsList) {
			logger.info("••• dataSource = " + ds);
		}

		logger.info("••• queues do PI");

		for (String q : MatrixDumpDataServiceDefinition.queues) {
			logger.info("••• queue = " + q);
			dsList = dataSourceService.getDataSourcesByQueueName(q);

			for (DataSource ds : dsList) {
				logger.info("••• PIDataSource = " + ds);
			}
		}
	}

	@Override
	protected void onLoadConfig(Map<String, Object> config) throws IllegalArgumentException {

	}

	@Override
	protected void onProcess(Waveform[] waveforms) throws Exception {
		for (Waveform waveform : waveforms) {
			logger.info("••• queue do PI: " + waveform.getQueueID());
		}
	}

	@Override
	public void setupPersistence() throws ApplicationException {
		dataSourceService = new DataSourceService(getContainer().getPersistenceUnitName());
	}
}
