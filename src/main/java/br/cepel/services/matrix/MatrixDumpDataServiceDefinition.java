package br.cepel.services.matrix;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Injector;

import br.cepel.common.pattern.event.EventListenerPriority;
import br.cepel.common.pattern.event.EventType;
import br.cepel.services.messor.MessorServiceDefinition;
import business.ApplicationException;

public class MatrixDumpDataServiceDefinition extends MessorServiceDefinition<MatrixDumpDataService> {
	private static Logger logger = Logger.getLogger(MatrixDumpDataServiceDefinition.class);
	public static final String[] queues = { "Queue_179_106_230_83_PI_POT123", "Queue_179_106_230_83_PI_POT345",
			"Queue_179_106_230_83_PI_POT678" };

	public MatrixDumpDataServiceDefinition() throws ApplicationException {
		super();
		serviceClass = MatrixDumpDataService.class;
	}

	@Override
	protected EventType getEvent() {
		return EventType.PUBLISH_MESSOR_DATA;
	}

	@Override
	protected void setupService(MatrixDumpDataService service) {
		// Nada a fazer
	}

	@Override
	protected Map<String, MatrixDumpDataService> createServiceInstances(Injector injector) throws ApplicationException {

		Map<String, MatrixDumpDataService> instances = new HashMap<>();
		boolean allow = Arrays.stream(MatrixDumpDataServiceDefinition.queues).map(q -> {
			String serviceName = getServiceClass().getSimpleName() + "@" + q;

			MatrixDumpDataService waveService = injector.getInstance(serviceClass);

			if (waveService != null) {

				try {
					waveService.setupPersistence();
					waveService.setName(serviceName);
					waveService.setTitle(serviceName);
					waveService.setEventType(getEvent());
					waveService.setScope(q);
					waveService.setPriority(EventListenerPriority.NORMAL);
					waveService.setEventType(EventType.PUBLISH_MESSOR_DATA);
					setupService(waveService);
					waveService.insertIntoManagement();
					instances.put(serviceName, waveService);
				} catch (ApplicationException e) {
					logger.error("Erro ao criar a instancia de servico '" + serviceName + "'.", e);
					return false;
				}
			} else {
				logger.error("Erro ao criar a instancia de servico '" + serviceName + "'.");
				return false;
			}
			return true;
		}).reduce(true, (x, y) -> x && y);
		if (!allow) {
			throw new ApplicationException("Nao foi possivel carregar as intancias da definicao '" + name + "'.");
		}

		return instances;
	}
}
