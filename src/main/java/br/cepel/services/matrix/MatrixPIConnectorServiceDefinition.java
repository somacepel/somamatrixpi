package br.cepel.services.matrix;

import br.cepel.services.FSWSimpleServiceDefinition;

public class MatrixPIConnectorServiceDefinition extends FSWSimpleServiceDefinition<MatrixPIConnectorService> {
	public MatrixPIConnectorServiceDefinition() {
		serviceClass = MatrixPIConnectorService.class;
	}
}
