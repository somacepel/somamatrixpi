package br.cepel.services.matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.log4j.Logger;

import br.cepel.common.meassurement.Waveform;
import br.cepel.common.pattern.event.Event;
import br.cepel.common.pattern.event.EventType;
import br.cepel.config.FSWConfig;
import br.cepel.fsw.FSWService;
import br.cepel.mngmt.inject.ManagedAttribute;
import br.cepel.services.online.report.MeassureDetail;

public class MatrixPIConnectorService extends FSWService {

	// IP : 179.106.230.83
	// Data Source=PI; Integrated Security=SSPI;
	enum PIJdbcType {
		PI_OLEDB_Enterprise("jdbc:pioledbent://_SERVER_/_CONNECTION_STRING_"), PI_OLEDB(
				"jdbc:pioledb://_SERVER_/_CONNECTION_STRING_"), PI_SQL(
						"jdbc:pisql://_SERVER_/_CONNECTION_STRING_"), PI_Integrator("jdbc:piintegrator://_SERVER_");

		public static String getPIUrl(PIJdbcType piType, String server, String connString) {
			String ret = piType.getUrlTemplate().replace("_SERVER_", server);
			if (piType == PIJdbcType.PI_Integrator) {
				return ret;
			} else {
				return ret.replace("_CONNECTION_STRING_", connString);
			}
		}

		public static String getPIUrl(String server) {
			return getPIUrl(PIJdbcType.PI_Integrator, server, "");
		}

		private String urlTemplate;

		private PIJdbcType(String urlTemplate) {
			this.urlTemplate = urlTemplate;
		}

		public String getUrlTemplate() {
			return urlTemplate;
		}
	}
	private static final String PI_SERVER = "179.106.230.83";
	private static final String PI_DATA_SOURCE = "PI";
	private static final String PI_CONN_STRING = "Data Source=" + PI_DATA_SOURCE + "; Integrated Security=SSPI;";
	private static Logger logger = Logger.getLogger(MatrixPIConnectorService.class);
	private boolean running = false;

	private List<PITagMetaInfo> existingTags = new ArrayList<>();

	PIJdbcType piJdbcType = PIJdbcType.PI_SQL;

	@ManagedAttribute(description = "piJdbcTypeCode especifica o piJdbcType: 0 -> PI_OLEDB_Enterprise, 1 -> PI_OLEDB, 2 -> PI_SQL, 3 -> PI_Integrator", isReadable = true, isWriteable = true)
	int piJdbcTypeCode = 0;

	// To use the PI OLEDB Enterprise table set and connect to PI Asset
	// Framework (AF).
	@ManagedAttribute(description = "Especifica a URL para se conectar ao PI", isReadable = true, isWriteable = false)
	private String jdbcUrl = PIJdbcType.getPIUrl(PIJdbcType.PI_SQL, PI_SERVER, PI_CONN_STRING);

	@ManagedAttribute(description = "Especifica as fontes de dado sendo lidas do PI", isReadable = true, isWriteable = false)
	private String piDataSources = "";

	private List<PITagMetaInfo> piTagMetaInfoList;
	private MatrixPIConnector matrixPIConnector = null;
	@Override
	protected void afterStart() throws Exception {

		while (running) {
			// Lê o snapshot e publica no Bus
			PIMeasureDetail[] snapshotReadings = matrixPIConnector.getDataSnapshot(existingTags, null);
			for (PIMeasureDetail reading : snapshotReadings) {
				String queueName = "Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + reading.getDataSourceName();
				queueName = queueName.replace(".", "_");
				publishWaveformArray(reading.getWaveforms(), queueName);
			}
			// String queueName = "Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE;
			Thread.sleep(5000);
		}
	}

	@Override
	protected void afterStop() throws Exception {
	}

	@Override
	protected void beforeStart() throws Exception {
		matrixPIConnector = new MatrixPIConnector(FSWConfig.getContainer(), this.jdbcUrl);
		matrixPIConnector.connect();
		checkTagConfiguration(piTagMetaInfoList);
		running = true;
	}

	@Override
	protected void beforeStop() throws Exception {
		running = false;
		matrixPIConnector.disconnect();
		matrixPIConnector.close();
		matrixPIConnector = null;
	}

	private void checkTagConfiguration(List<PITagMetaInfo> piTagMetaInfoList) {
		List<String> dataSourceList = matrixPIConnector.getTagList();
		for (PITagMetaInfo piTagMetaInfo : piTagMetaInfoList) {
			if (!dataSourceList.contains(piTagMetaInfo.getTag())) {
				logger.warn("TAG " + piTagMetaInfo.getTag() + " não encontrada no PI.");
			} else {
				existingTags.add(piTagMetaInfo);
			}
		}
	}

	public PIMeasureDetail[] getDataFlowWindow(Predicate<? super PIMeasureDetail> filter, Long initialTimestamp,
			Integer size) {
		if (filter == null) {
			throw new RuntimeException("••• Filtro não pode ser nulo");
		}
		// Implementar o restante
		return null;
	}

	public PIMeasureDetail[] getDataFlowWindow(StringPredicate filter, Long initialTimestamp, Integer size) {
		if (filter == null) {
			throw new RuntimeException("••• Filtro não pode ser nulo");
		}
		// Implementar o restante
		return null;
	}

	private String getJdbcUrl() {
		return jdbcUrl;
	}

	private String getPiDataSources() {
		return this.piDataSources;
	}

	private PIJdbcType getPiJdbcType() {
		MeassureDetail m;
		return piJdbcType;
	}

	@Override
	protected void onClearStatistics() {
	}

	@Override
	protected void onLoadConfig(Map<String, Object> config) throws IllegalArgumentException {
		piTagMetaInfoList = new ArrayList<>();
		piTagMetaInfoList
				.add(new PITagMetaInfo("POT123", "CDEP158", "Potência Ativa", PITagMetaInfo.Type.HISTORY_DATA,
						"Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + "POT123".replace(".", "_")));
		piTagMetaInfoList
				.add(new PITagMetaInfo("POT345", "CDM158", "Potência Reativa", PITagMetaInfo.Type.HISTORY_DATA,
						"Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + "POT345".replace(".", "_")));

		piTagMetaInfoList.add(new PITagMetaInfo("POT678", "CDT158", "Potência Aparente", PITagMetaInfo.Type.RAW_DATA,
				"Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + "POT678".replace(".", "_")));
		piTagMetaInfoList
				.add(new PITagMetaInfo("POT658", "EsseNãoExiste", "Potência Aparente", PITagMetaInfo.Type.RAW_DATA,
						"Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + "POT658".replace(".", "_")));
		piTagMetaInfoList.add(
				new PITagMetaInfo("POT7878", "EsseTambémNãoExiste", "Potência Aparente", PITagMetaInfo.Type.RAW_DATA,
						"Queue_" + PI_SERVER + "_" + PI_DATA_SOURCE + "_" + "POT7878".replace(".", "_")));

		System.out.println("••• Com PIJdbcType " + PIJdbcType.PI_SQL + " piJdbcTypeCode = " + getPiJdbcType().ordinal()
				+ " e jdbcUrl = '" + jdbcUrl + "'. Temos também DataSources = " + getPiDataSources());
	}

	@SuppressWarnings("unused")
	private void printPIJdbcTypeDetails(PIJdbcType piType) {
		switch (piType) {
		case PI_OLEDB_Enterprise:
			System.out.println("••• " + PIJdbcType.PI_OLEDB_Enterprise + " O ordinal = " + piType.ordinal());
			break;
		case PI_OLEDB:
			System.out.println("••• " + PIJdbcType.PI_OLEDB + " O ordinal = " + piType.ordinal());
			break;
		case PI_SQL:
			System.out.println("••• " + PIJdbcType.PI_SQL + " O ordinal = " + piType.ordinal());
			break;
		case PI_Integrator:
			System.out.println("••• " + PIJdbcType.PI_Integrator + " O ordinal = " + piType.ordinal());
			break;
		default:
			throw new RuntimeException("Valor invalido");
		}
	}

	public void publishData(String topic) {
		// Implementar
	}

	public void publishWaveformArray(Waveform[] waveformArray, String queueName) {
		logger.info("••• Publicando: queueName = " + queueName + ", waveformArray.length = " + waveformArray.length);
		getContainer().getEventBus().broadCastEvent(new Event(EventType.PUBLISH_MESSOR_DATA, queueName, waveformArray));
	}
}

class PIMeasureDetail {
	public static String PI_DECIMALS_QTY_ENV;
	public static int PI_DECIMALS_QTY;
	static {
		try {
			PI_DECIMALS_QTY_ENV = System.getenv("PI_DECIMALS_QTY");
			PI_DECIMALS_QTY = Integer.parseInt(PI_DECIMALS_QTY_ENV);
		} catch (Exception e) {
			System.out.println("••• " + e.getLocalizedMessage());
			PI_DECIMALS_QTY_ENV = "7";
			PI_DECIMALS_QTY = 7;
		}
	}
	private String dataSourceName;
	private String dataSourceTitle;
	private String dataSourceOrigin;

	private Waveform[] waveforms;

	public PIMeasureDetail(String dataSourceName, String dataSourceTitle, String dataSourceOrigin,
			Waveform... waveforms) {
		super();
		this.dataSourceName = dataSourceName;
		this.dataSourceTitle = dataSourceTitle;
		this.dataSourceOrigin = dataSourceOrigin;
		this.waveforms = waveforms;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public String getDataSourceOrigin() {
		return dataSourceOrigin;
	}

	public String getDataSourceTitle() {
		return dataSourceTitle;
	}

	public Waveform[] getWaveforms() {
		return waveforms;
	}
}

class PITagMetaInfo {
	enum Type {
		HISTORY_DATA, RAW_DATA
	}

	// PotênciaAtiva=POT123|Potência Ativa|historyData
	String dataSource;
	String tag;
	String title;
	Type type;
	String queueId;

	public PITagMetaInfo(String dataSource, String tag, String title, Type type, String queueId) {
		super();
		this.dataSource = dataSource;
		this.tag = tag;
		this.title = title;
		this.type = type;
		this.queueId = queueId;
	}

	public String getDataSource() {
		return dataSource;
	}

	public String getTag() {
		return tag;
	}

	public String getTitle() {
		return title;
	}

	public Type getType() {
		return type;
	}

	public String getQueueId() {
		return queueId;
	}
}
