package br.cepel.restful.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import br.cepel.common.meassurement.Waveform;
import br.cepel.services.online.report.MeassureDetail;
import br.cepel.services.online.report.MeassureDetailsList;

public class TAFReport implements Serializable {
	private static final long serialVersionUID = 4936529113260357306L;

	private static Logger logger = Logger.getLogger(TAFReport.class);

	List<Queue> queues = new ArrayList<>();

	Queue add(Queue q) {
		queues.add(q);
		return q;
	}

	public static TAFReport buildReport(Long refId, Map<String, MeassureDetailsList> meassures) {
		TAFReport report = new TAFReport();
		for (Entry<String, MeassureDetailsList> entry : meassures.entrySet()) {
			MeassureDetailsList mdl = entry.getValue();
			Queue q = new Queue(mdl.queueName);
			report.add(q);
			List<MeassureDetail> ml = mdl.meassures;
			for (MeassureDetail meassureDetail : ml) {
				String dsName = meassureDetail.getDataSourceName();
				Double[] data = getValuesFromWaveform(meassureDetail.getWaveforms());
				Parts p = new Parts(dsName, data);
				q.add(p);
			}
		}
		return report;
	}

	private static Double[] getValuesFromWaveform(Waveform[] waveforms) {
		List<Double> data = new ArrayList<>();
		Double[] x = new Double[0];
		for (Waveform waveform : waveforms) {
			double[] valores = waveform.getValues();
			for (Double v : valores) {
				data.add(v);
			}
		}
		logger.info("data = " + data);
		return data.toArray(x);
	}
}

class Queue {
	String name;
	List<Parts> subParts = new ArrayList<>();;

	public Queue(String name) {
		this.name = name;
	}

	Parts add(Parts p) {
		subParts.add(p);
		return p;
	}
}

class Parts {
	String dsName;
	Double[] data;

	public Parts() {
	}

	Parts(String dsName, Double[] data) {
		super();
		this.dsName = dsName;
		this.data = data;
	}
}

// { "queue" : { "name" : "FILA 1", "subParts" : [ { "dsName" : "MIV-098-RJ",
// "data" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ] }, { "dsName" : "MIV-098-RJ",
// "data" : [ 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 ] }, { "dsName" :
// "MIV-098-RJ", "data" : [ 31, 31, 31, 31, 31, 31, 31, 31, 31, 31 ] } ] } }
