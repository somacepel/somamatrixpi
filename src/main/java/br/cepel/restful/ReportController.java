package br.cepel.restful;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.cepel.config.FSWConfig;
import br.cepel.restful.vo.TAFReport;
import br.cepel.services.online.report.MeassureDetailsList;
import br.cepel.services.report.ReportService;

@RestController
public class ReportController {
	private static Logger logger = Logger.getLogger(ReportController.class);

	@RequestMapping("/report")
	public String report(@RequestParam(value = "refId", defaultValue = "0") Long refId) {
		// response.setContentType("application/json");
		// response.setBufferSize(8192);
		StringBuffer sb = new StringBuffer("");
		String jsonString = "";
		String ret = "{\n" + "  \"queue\" : {\n" + "    \"name\" : \"FILA 1\",\n" + "    \"subParts\" : [ {\n"
				+ "      \"dsName\" : \"MIV-098-RJ\",\n" + "      \"data\" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]\n"
				+ "    }, {\n" + "      \"dsName\" : \"MIV-098-RJ\",\n"
				+ "      \"data\" : [ 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 ]\n" + "    }, {\n"
				+ "      \"dsName\" : \"MIV-098-RJ\",\n"
				+ "      \"data\" : [ 31, 31, 31, 31, 31, 31, 31, 31, 31, 31 ]\n" + "    } ]\n" + "  }\n" + "}\n";

		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		try {
			ReportService rs = FSWConfig.getContainer().getService(ReportService.class);
			Map<String, MeassureDetailsList> meassures;
			meassures = rs.getAllMeassureDetails(refId, null);
			TAFReport report = TAFReport.buildReport(refId, meassures);
			TAFReportWrapper trw = new TAFReportWrapper(report);
			trw.setErrorCodeAndErrorMessage(0, "OK");
			// Object to JSON in String
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(trw);
			logger.info(jsonString);
			sb = rs.saveReportsToFiles(refId);
		} catch (IllegalArgumentException e) {
			TAFReportWrapper trw = new TAFReportWrapper();
			trw.setErrorCodeAndErrorMessage(2, "" + e.getLocalizedMessage());
			logger.error("Erro processando requisicao de solicitacao de relatorio", e);
			jsonString = serializeAndCheckForError(trw);
		} catch (JsonMappingException e) {
			TAFReportWrapper trw = new TAFReportWrapper();
			trw.setErrorCodeAndErrorMessage(3, "" + e.getLocalizedMessage());
			e.printStackTrace();
			jsonString = serializeAndCheckForError(trw);
		} catch (JsonProcessingException e) {
			TAFReportWrapper trw = new TAFReportWrapper();
			trw.setErrorCodeAndErrorMessage(4, "" + e.getLocalizedMessage());
			e.printStackTrace();
			jsonString = serializeAndCheckForError(trw);
		}
		logger.info(sb.toString());
		// return sb.toString();
		return jsonString;
	}

	private static String serializeAndCheckForError(TAFReportWrapper trw) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		String jsonString = "";
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(trw);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		return jsonString;
	}
}

class TAFReportWrapper {
	TAFReport report = new TAFReport();
	int errorCode = 1; // errorCode == 1 significa "Report Vazio"
	String errorMessage = "Report Vazio";

	public TAFReportWrapper() {
	}

	public TAFReportWrapper(TAFReport report) {
		this.report = report;
	}

	public void setErrorCodeAndErrorMessage(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
