package br.cepel.restful;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.cepel.restful.vo.Greeting;

@RestController
public class GreetingController {
	private static Logger logger = Logger.getLogger(GreetingController.class);
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		logger.info("greeting Servlet com o parametro name = '" + name + "'");
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}
}
