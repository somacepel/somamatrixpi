package br.cepel;

import org.apache.log4j.Logger;

/**
 * Hello world Application!
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.cepel.config.FSWConfig;
import br.cepel.config.PathAndLogModuleConfig;
import br.cepel.fsw.config.ModuleConfig;

@SpringBootApplication
public class App {
	private static Logger logger = Logger.getLogger(App.class);

	// private static PersistenceUnitName persistenceUnit =
	// PersistenceUnitName.SOMA_PERSISTENCE;
	private static final ModuleConfig fswConfig = new FSWConfig();

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Hello World Application!");
		ModuleConfig pathAndLog = new PathAndLogModuleConfig();
		pathAndLog.setup();

		logger.info("*************************************");
		logger.info("        Iniciando o FSW    ");
		logger.info("*************************************\n");

		try {
			fswConfig.setup();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}

		logger.info("*************************************");
		logger.info("          FSW iniciado  ");
		logger.info("*************************************\n");
		SpringApplication.run(App.class, args);
		logger.info("****** DORMIREI POR 20 SEGUNDOS *******\n");
		Thread.sleep(20000);
		logger.info("******          ACORDEI         *******\n");
	}
}
