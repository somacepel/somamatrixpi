# somaMatrixPI

## Instalando o projeto

São apenas cinco comandos na console (Terminal)

```bash
cd ~/Desktop/Development # ou outro diretório a critério do Desenvolvedor
git clone git@bitbucket.org:somaelements/somamatrixpi.git somaMatrixPI
cd somaMatrixPI
mvn install
./runSomaMatrixPI
```

Caso tenha problemas com o diretório `.mvn`, execute:

```bash
mvn -N io.takari:maven:wrapper
```
### Testando o ambiente

No Ubuntu ou macOS faça:

```bash
open http://localhost:8080//greeting?name=joão
```

No Windows abra o Browser em [http://localhost:8080//greeting?name=joão](http://localhost:8080//greeting?name=joão)

Faça refresh no browser para observar o `id` aumentando de um a cada refresh.

## Desenvolvendo

> . . .


Após a alteração desejada no Fonte execute a geração de relatório para verificar 
se os serviços foram carregados e se é possível extrair relatórios 

```bash
open http://localhost:8080///report?refId=0

```

## Instalando no SOMA / MATRIX para produção

> TBD

